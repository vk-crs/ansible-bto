# Ansible Setup

### Overview

* An Ansible script to restore an SQL Server database from a backup file stored on Azure Blob Storage

### Pre-requisites

* Apply Terraform templates to provision IaaS and let it cool down for about 5 minutes
* Preserve Terraform output for values required to run Ansible e.g.: ip address, ssh password, and etc.
* [Install Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#)
* Upload the [db backup file](./misc/helloworld.bak) to an Azure StorageV2, ensure blob type selected `Page Blob`
* Install 'pywinrm[credssp]': `python3 -m pip install pywinrm[credssp]`, not supported on Azure Shell
* You will need Azure storage account key under the "Access Keys"
* Clone the repository with Ansible installed, please note: Windows is not supported for the control node
* Navigate to the repository directory using `cd` command

### Deploy

* Review/update `./roles/sql/vars/main.yml` file
* Configure ansible vault to store secrets: `ansible-vault create ./roles/sql/vars/passwords.yml`
* You can also edit an existing secrets file: `ansible-vault edit ./roles/sql/vars/passwords.yml`
* Content of the `./roles/sql/vars/passwords.yml` file:
```
backup_device_key: {your storage account key from Azure Portal}
ssh_pass: {vm password from terraform output}
```
* On MacOS, only export the variable: `export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES`
* Run the Ansible playbook: `ansible-playbook configure.yml -vvvv -i hosts --ask-vault-pass`

### Verify

* Login into the vm and launch PowerShell
* Run command to confirm content of the table restored: `sqlcmd -Q "select top 10 * from HelloWorld.dbo.Messages"