param (
   [Parameter(Mandatory=$true)][string]$vm_hostname,
   [Parameter(Mandatory=$true)][string]$backup_device_url,
   [Parameter(Mandatory=$true)][string]$backup_device_accountname,
   [Parameter(Mandatory=$true)][string]$backup_device_key,
   [Parameter(Mandatory=$true)][string]$database_name,
   [Parameter(Mandatory=$true)][string]$backup_block_size,
   [Parameter(Mandatory=$true)][string]$data_file_location,
   [Parameter(Mandatory=$true)][string]$log_file_location,
   [Parameter(Mandatory=$true)][string]$data_file_name,
   [Parameter(Mandatory=$true)][string]$log_file_name,
   [Parameter(Mandatory=$true)][string]$disk_sqldata_letter,
   [Parameter(Mandatory=$true)][string]$disk_sqllogs_letter
)

function Restore-SQLDataBase() {
    Param (
        [Parameter(Mandatory=$true)][string]$SQLServer,
        [Parameter(Mandatory=$true)][string]$DatabaseName,
        [Parameter(Mandatory=$true)][string]$DataFileLocation,
        [Parameter(Mandatory=$true)][string]$LogFileLocation,
        [Parameter(Mandatory=$true)][string]$DataFileName,
        [Parameter(Mandatory=$true)][string]$LogFileName,
        [Parameter(Mandatory=$true)][string]$BackUpDeviceUrl,
        [Parameter(Mandatory=$true)][string]$BackupBlockSize,
        [Parameter(Mandatory=$true)][string]$BackupDeviceAccountName,
        [Parameter(Mandatory=$true)][string]$BackupDeviceKey
    )

    md -Force (Split-Path -Path $LogFileLocation -Parent)
    md -Force (Split-Path -Path $DataFileLocation -Parent)

    # Import necessary SQL Modules
    Import-Module SQLPS

   # Wait for SQL Service to be UP
   LogMessage -Message "Wait for SQL Service to be ready... $SQLServer";
   $ew = new-object system.management.ManagementEventWatcher
   $ew.query = "Select * From __InstanceCreationEvent Where TargetInstance ISA 'Win32_NTLogEvent'"
   while(!(get-eventlog -logname 'Application' -Source 'MSSQLSERVER' | ? {$_.EventId -eq 17126})){
      LogMessage -Message "Still waiting for SQL Service to be ready...";
	    $ew.WaitForNextEvent()
   }

   $credentialsName = "RestoreBackupCredentials"
   LogMessage -Message "Verify if credentials $credentialsName exist";

   If(-Not (Test-Path SQLSERVER:\SQL\$SQLServer\DEFAULT\Credentials\$credentialsName)) {
     # Create SQL Server Credentials that will be used to restore the DB.
     LogMessage -Message "Creating new credentials with name $credentialsName";
     $secret = ConvertTo-SecureString $BackupDeviceKey -AsPlainText -Force
     New-SqlCredential -Name "$credentialsName" -Identity $BackupDeviceAccountName -Secret $secret -Path SQLSERVER:\SQL\$SQLServer\DEFAULT\Credentials
   }

    # Create realocation objects for Log and Data
    $sqlServerSnapinVersion = (Get-Command SQLPS\Restore-SqlDatabase).ImplementingType.Assembly.GetName().Version.ToString()
    $assemblySqlServerSmoExtendedFullName = "Microsoft.SqlServer.SmoExtended, Version=$sqlServerSnapinVersion, Culture=neutral, PublicKeyToken=89845dcd8080cc91"

    LogMessage -Message "Creating realocation Log and Data objects in Data: $DataFileLocation and Log: $LogFileLocation";
    $RelocateData = New-Object "Microsoft.SqlServer.Management.Smo.RelocateFile, $assemblySqlServerSmoExtendedFullName"($DataFileName, $DataFileLocation)
    $RelocateLog = New-Object "Microsoft.SqlServer.Management.Smo.RelocateFile, $assemblySqlServerSmoExtendedFullName"($LogFileName, $LogFileLocation)

    # Execute Command to restore the DB
    LogMessage -Message "Restoring $DatabaseName Database into $SQLServer";
    SQLPS\Restore-SqlDatabase -ServerInstance $SQLServer -Database $DatabaseName -BackupFile $BackUpDeviceUrl -RelocateFile @($RelocateData,$RelocateLog) -SqlCredential $credentialsName -BlockSize $BackupBlockSize -ReplaceDatabase

    LogMessage -Message "Restore Database completed successfully";
}

function Prepare-Disk() {
  Param ([Parameter(Mandatory=$true)][string]$disk_sqldata_letter,
         [Parameter(Mandatory=$true)][string]$disk_sqllogs_letter
       )

    Get-Disk | Where partitionstyle -eq 'raw' | Initialize-Disk -PartitionStyle MBR -PassThru | New-Partition -AssignDriveLetter -UseMaximumSize | Format-Volume -FileSystem NTFS -Confirm:$false;
    Set-Volume -DriveLetter ${disk_sqldata_letter}  -NewFileSystemLabel "SQL Data";
    Set-Volume -DriveLetter ${disk_sqllogs_letter}  -NewFileSystemLabel "SQL Logs";
}

function LogMessage(){
Param ([Parameter(Mandatory=$true)][string]$Message)
    Write-Output $Message;
}

try {
  # Prepare Log and Data Partitions
  Prepare-Disk -disk_sqldata_letter "${disk_sqldata_letter}" -disk_sqllogs_letter "${disk_sqllogs_letter}"
  # Restore Database
  Restore-SQLDataBase -SQLServer "${vm_hostname}" -BackupDeviceAccountName "${backup_device_accountname}" -BackupDeviceKey "${backup_device_key}" -BackupBlockSize "${backup_block_size}"  -BackUpDeviceUrl "${backup_device_url}"  -DatabaseName "${database_name}" -DataFileName "${data_file_name}" -DataFileLocation "${data_file_location}" -LogFileName "${log_file_name}" -LogFileLocation "${log_file_location}";
  
  exit 0;
}
Catch {
  Write-Host "An error occurred.";
  Write-Host $_;
  exit 1;
};
